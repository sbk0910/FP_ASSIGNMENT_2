#include <iostream>
#include <list>
#include <iterator>

#define BF 4

#if BF%2
#define M BF/2+1
#else
#define M BF/2
#endif

#ifndef BPLUS_TREE_H_INCLUDED
#define BPLUS_TREE_H_INCLUDED

using namespace std;

class BPT_Node {
public:
	list<int> score;
    list<int> blockNum;
	int entry_num = 0;
	bool root = false;
	bool leaf = false;
	BPT_Node *next = NULL;	// only leaf
	BPT_Node *parent = NULL;
	list<BPT_Node*> child;   // only internal

	BPT_Node(bool _leaf, bool _root, int _score);
	int insert_Entry(int _score);
};

class BPT {
public:
	BPT_Node* root;

	BPT(int _score);

	BPT_Node* find_Left(BPT_Node* tmp);
	BPT_Node* find_Node(int _score);
	void split_leafNode(BPT_Node *tmp);
	void split_internalNode(BPT_Node *tmp);
	void insert_Record(int _score);
	void showKthLeaf(int k);
	void PrintLeaf();
	void Print_Tree();
	void Print_Node(BPT_Node* tmp);
	};

void Int_ListSplice(list<int>* score,list<int> *l_score, list<int> *r_score);

void Node_ListSplice(list<BPT_Node*>* child, list<BPT_Node*> *l_child, list<BPT_Node*> *r_child);



#endif // BPLUS_TREE_H_INCLUDED
