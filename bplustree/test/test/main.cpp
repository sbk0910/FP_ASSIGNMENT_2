#include <iostream>
#include "bplus_tree.h"

using namespace std;

int main() {
	BPT* studentBPT = new BPT(10);

	for (int i = 2; i < 30; i += 2) {
		studentBPT->insert_Record(i * 10);
	}
	for (int i = 3; i < 30; i += 2) {
		studentBPT->insert_Record(i * 10);
	}
	studentBPT->insert_Record(100);

	studentBPT->PrintLeaf();
	studentBPT->Print_Tree();
}