#include <iostream>
#include <list>
#include <iterator>
#include "bplus_tree.h"



BPT_Node::BPT_Node(bool _leaf, bool _root, int _score) {
	leaf = _leaf;
	root = _root;
	score.push_back(_score);
	entry_num = 1;

}
int BPT_Node::insert_Entry(int _score) {
	cout << "insert_Entry" << endl;
	int cn = 0;
	list<int>::iterator it = score.begin();
	while (it != score.end()) {
		if (*it < _score) {
			it++;
			cn++;
		}
		else {
			break;
		}
	}
	score.insert(it, _score);
	entry_num++;
	return cn;
}

BPT::BPT(int _score) {
	cout << "BPT Constructor" << endl;
	root = new BPT_Node(true, true, _score);
}

BPT_Node*  BPT::find_Left(BPT_Node* tmp) {
	if (tmp->leaf) {
		cout << "It's leaf!" << endl;
		return tmp;
	}
	else {
		cout << "It's not leaf!" << endl;
		return find_Left(*tmp->child.begin());
	}
}

BPT_Node* BPT::find_Node(int _score) {
	cout << "find_Node" << endl;
	BPT_Node* tmp = find_Left(root);
	cout << "tmp: " << tmp << endl;
	list<int>::iterator it = tmp->score.begin();
	BPT_Node* prev = NULL;
	while (tmp != nullptr) {
		cout << "*it: " << *it << endl;
		it = tmp->score.end();
		it--;
		if (*it < _score) {
			cout << "*it<_score" << endl;
			prev = tmp;
			tmp = tmp->next;
			if (tmp != nullptr && *(tmp->score.begin()) > _score) {
				cout << "im hear" << endl;
				return prev;
			}
			
		}
		else {
			cout << "*it>_score" << endl;
			return tmp;
		}
	}
	return prev;
}

void  BPT::split_internalNode(BPT_Node *tmp) {
	// score split
	list<int> l_score;
	list<int> r_score;
	list<int>::iterator s_lit = tmp->score.begin();
	advance(s_lit, M);
	int key = *s_lit;
	Int_ListSplice(&(tmp->score), &l_score, &r_score);

	// child split
	list<BPT_Node*> l_child;
	list<BPT_Node*> r_child;
	list<BPT_Node*>::iterator c_lit = tmp->child.begin();
	advance(c_lit, M + 1);
	Node_ListSplice(&(tmp->child), &l_child, &r_child);

	// split left & right node
	tmp->score = l_score;
	tmp->entry_num = l_score.size();
	tmp->child = l_child;
	BPT_Node *r_node = new BPT_Node(false, false, 0);
	r_node->score = r_score;
	r_node->entry_num = r_score.size();
	r_node->child = r_child;
	

	list<BPT_Node*>::iterator cit = r_node->child.begin();
	for (cit = r_node->child.begin(); cit != r_node->child.end(); cit++) {
		BPT_Node* Ctmp;
		Ctmp = *cit;
		Ctmp->parent = r_node;
	}


	if (tmp->root) {
		tmp->root = false;
		BPT_Node *new_root = new BPT_Node(false, true, key);
		root = new_root;
		tmp->parent = new_root;
		r_node->parent = new_root;
		new_root->child.push_back(tmp);
		new_root->child.push_back(r_node);
	}
	else {
		int c_pos = tmp->parent->insert_Entry(key);
		r_node->parent = tmp->parent;
		c_lit = tmp->parent->child.begin();
		advance(c_lit, c_pos);
		if (tmp->parent->root) {
			c_lit++;
		}
		tmp->parent->child.insert(c_lit, r_node);
		if (tmp->parent->entry_num > BF) {
			split_internalNode(tmp->parent);
		}
	}
}


void BPT::insert_Record(int _score) {
	cout << endl << endl <<  "insert_Record " << _score <<  endl;
	BPT_Node* seat = find_Node(_score);
	cout << "Finish find_Node" << endl;
	seat->insert_Entry(_score);
	if (seat->entry_num > BF) {
		split_leafNode(seat);
	}
}

void BPT::showKthLeaf(int k) {
	BPT_Node *want = find_Left(root);
	for (int i = 0; i < k - 1; i++) {
		if (want->next != NULL)
			want = want->next;
		else {
			cout << "It's not exist!" << endl;
			break;
		}
	}
	list<int>::iterator it = want->score.begin();
	for (it = want->score.begin(); it != want->score.end(); it++) {
		cout << *it << "->" << endl;
	}
}

void BPT::split_leafNode(BPT_Node *tmp) {
	list<int> l_score;
	list<int> r_score;
	list<int>::iterator s_lit = tmp->score.begin();
	advance(s_lit, M);
	int key = *s_lit;
	Int_ListSplice(&(tmp->score), &l_score, &r_score);
	if (tmp->root) {
		BPT_Node *new_root = new BPT_Node(false, true, key);
		BPT_Node *new_r = new BPT_Node(true, false, key);

		tmp->root = false;
		tmp->score.assign(l_score.begin(), l_score.end());
		tmp->entry_num = l_score.size();
		tmp->parent = new_root;
		tmp->next = new_r;

		root = new_root;
		new_r->parent = new_root;
		new_r->score.assign(r_score.begin(), r_score.end());
		new_r->entry_num = r_score.size();

		new_root->child.push_back(tmp);
		new_root->child.push_back(new_r);
		
	}
	else {
		BPT_Node *new_r = new BPT_Node(true, false, key);
		new_r->parent = tmp->parent;
		new_r->score.assign(r_score.begin(), r_score.end());
		new_r->entry_num = r_score.size();
		new_r->next = tmp->next;

		tmp->score.assign(l_score.begin(), l_score.end());
		tmp->next = new_r;
		tmp->entry_num = l_score.size();

		list<BPT_Node*>::iterator c_lit = tmp->parent->child.begin();
		int c_pos = tmp->parent->insert_Entry(key);
		advance(c_lit, c_pos);
		if (tmp->parent->root) {
			c_lit++;
		}
		new_r->parent->child.insert(c_lit, new_r);

		if (tmp->parent->entry_num > BF) {
			split_internalNode(tmp->parent);
		}
	}
}

void BPT::PrintLeaf() {
	BPT_Node *left = find_Left(root);
	while (left != nullptr) {
		list<int>::iterator lit = left->score.begin();
		for (lit = left->score.begin(); lit != left->score.end(); lit++) {
			cout << *lit << "\t";
		}
		cout << endl;
		left = left->next;
	}
}


void Int_ListSplice(list<int>* score, list<int> *l_score, list<int> *r_score) {
	list<int>::iterator lit = score->begin();
	for (int i = 0; i < score->size(); i++) {
		if (i < M) {
			l_score->push_back(*lit);
		}
		else {
			r_score->push_back(*lit);
		}
		lit++;
	}
}

void Node_ListSplice(list<BPT_Node*>* child, list<BPT_Node*> *l_child, list<BPT_Node*> *r_child) {
	list<BPT_Node*>::iterator lit = child->begin();
	int num;
	if (child->size() == BF+1) {
		num = M;
	}
	else {
		num = M + 1;
	}
	for (int i = 0; i < child->size(); i++) {
		if (i < num) {
			l_child->push_back(*lit);
		}
		else {
			r_child->push_back(*lit);
		}
		lit++;
	}
}

void BPT::Print_Tree() {
	cout << "Print Root Node " << endl;
	Print_Node(root);
}

void BPT::Print_Node(BPT_Node* tmp) {
	if (tmp->leaf) {
		cout << "Print Leaf Node" << endl;
		list<int>::iterator lit = tmp->score.begin();
		for (lit = tmp->score.begin(); lit != tmp->score.end(); lit++) {
			cout << *lit << "  ";
		}
		cout << endl;
		cout << "------------------------------" << endl;
	}
	else {
		cout << "Print InterNal Node" << endl;
		list<int>::iterator lit = tmp->score.begin();
		for (lit = tmp->score.begin(); lit != tmp->score.end(); lit++) {
			cout << *lit << "  ";
		}
		cout << endl;
		cout << "------------------------------" << endl;
		list<BPT_Node*>::iterator cit = tmp->child.begin();
		for (cit = tmp->child.begin(); cit != tmp->child.end(); cit++) {
			Print_Node(*cit);
		}
	}
}